package main

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"log"
)

func main() {


	topic := "learn_golang2"

	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers" : "localhost:9092"})

	if err != nil {
		if kafkaError, ok := err.(kafka.Error); ok == true {
			switch errCode := kafkaError.Code(); errCode {
			case kafka.ErrInvalidArg:
				log.Fatalf("Can't create the producer because you've configured it wrong, code: %d", errCode)
			default:
				log.Fatalf("Can't create the producer, code: %d for: %s", errCode, err)
			}
		} else {
			log.Fatalf("😢 Oh noes, there's a generic error creating the Producer! %v",
				err.Error())
		}
	}
	// sigterm from main to go routine
	termChan := make(chan bool, 1)
	// sigterm termination done from goroutine
	doneChan := make(chan bool)

	// build message
	msg := kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic: &topic,
			Partition: 0,
		},
		Value: []byte("Happy learn"),
	}

	// handle any events comes in
	go func() {
		doTerm := false
		for !doTerm {
			select {
			case event := <- p.Events() :
				switch event.(type) {
				case *kafka.Message:
					// delivery report
					kmsg := event.(*kafka.Message)
					if kmsg.TopicPartition.Error != nil {
						log.Fatalf("failed to send message: %v to topic: %v - error: %v",
							string(kmsg.Value),
							string(*kmsg.TopicPartition.Topic),
							kmsg.TopicPartition.Error)
					} else {
						log.Printf("✅ Message '%v' delivered to topic '%v' (partition %d at offset %d)\n",
							string(kmsg.Value),
							string(*kmsg.TopicPartition.Topic),
							kmsg.TopicPartition.Partition,
							kmsg.TopicPartition.Offset)
					}
				case kafka.Error:
					// an error
					errorMsg := event.(kafka.Error)
					log.Fatalf("caught an error %v", errorMsg)
				default:
					log.Fatalf("Not a message or error: %v", event)

				}

			case <- termChan:
				doTerm = true
			}
		}

		close(doneChan)
	}()

	// produce message
	if err := p.Produce(&msg, nil); err != nil {
		fmt.Printf("😢 Darn, there's an error producing the message! %v", err.Error())
	}

	t := 1000
	if r := p.Flush(t); r > 0 {
		fmt.Printf("\n--\n⚠️ Failed to flush all messages after %d milliseconds. %d message(s) remain\n", t, r)
	} else {
		fmt.Println("\n--\n✨ All messages flushed from the queue")
	}

	// --
	// Stop listening to events and close the producer
	// We're ready to finish
	termChan <- true
	// wait for go-routine to terminate
	<-doneChan
	// Now we can exit

	p.Close()


}
