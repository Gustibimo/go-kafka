package main

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"log"
)

func main() {


	topic := "learn_golang"

	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers" : "localhost:9092"})

	if err != nil {
		log.Fatalf("Error producer")
	}

	p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic: &topic,
			Partition: 0,
		},
		Value: []byte("Happy learn"),
	}, nil)

	p.Close()


}
